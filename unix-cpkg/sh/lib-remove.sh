if [ ! -d lib ]; then
	echo -e "${ERROR}: not a project directory"
	endscript
fi

if [ ! -d include ]; then
	echo -e "${ERROR}: not a project directory"
	endscript
fi

if [ ! -d doc ]; then
	echo -e "${ERROR}: not a project directory"
	endscript
fi

find include -type f -name "${LIBNAME}.h" -delete

find lib -type d -name "${LIBNAME}" | while read -r lib
do
	rm -rf "$lib"
done

find doc/lib -type d -name "${LIBNAME}" | while read -r lib
do
	rm -rf "$lib"
done

sed -i "/${LIBNAME}/d" ".cpkg/installed"

echo "done"

endscript

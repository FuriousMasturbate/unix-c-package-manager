PKGLIST=${CONF}/pkg-list-dir/pkg-list

#check if LIBLISTFILE exists
if [ ! -f "$PKGLIST" ]; then
	echo -e "${ERROR}: could not find $PKGLIST"
	endscript
fi

#remove directory contents temp/liblist if it exists
rm -rf ${TEMP}/libs || { echo "${ERROR}: rm: failed to execute rm -rf temp/libs" ; endscript; }

#make temp folder
mkdir -p ${TEMP}/libs || { echo "${ERROR}: could not make directory temp/libs" ; endscript; }

#download library
sed '/:/!d;/^ *#/d;s/:/ /;' < "$PKGLIST" | while read -r libname libremote
do
	if [ "$libname" = "$LIBNAME" ]; then

		LIBDIR=${TEMP}/libs/"$libname"
		mkdir "$LIBDIR"

		git clone "$libremote" "$LIBDIR" || { echo -e "${ERROR}: git: could not clone $libremote" ; endscript; }

		break
	fi
done

mkdir -p include

#install library
find ${TEMP}/libs -type d -name "public-headers" | while read -r publicheaderspath
do
	cp "$publicheaderspath"/* include
done

find ${TEMP}/libs -type d -name "src" | while read -r srcpath
do
	mkdir -p lib/"$LIBNAME"
	cp "$srcpath"/* lib/"$LIBNAME"
done

find ${TEMP}/libs -type d -name "doc" | while read -r docpath
do
	mkdir -p doc/lib/"$LIBNAME"
	cp "$docpath"/* doc/lib/"$LIBNAME"
done

sed -i '/${LIBNAME}/d' .cpkg/installed
echo "$LIBNAME" >> .cpkg/installed

echo "done"

endscript

CFGFILE=${CONF}/lib-update.conf

echo "looking for lib-update.conf ..."

#check if CFGFILE file exists
if [ -f "$CFGFILE" ]; then
    echo "found 'lib-update.conf'"
else
	echo -e "${ERROR}: could not find 'lib-update.conf'"
	endscript
fi



echo "reading lib-update.conf ..."

#read config file and assign values to variables
#variable(s) that are expected to be fetched from config file:
#LIST_REMOTE, LIST_FILENAME
eval $(echo "$(format-file "$CFGFILE")" | sed '/:/!d;/^ *#/d;s/:/ /;' | while read -r key val
do
    str="$key='$val'"
    echo "$str"
done)

echo "fetching list of available libraries..."



rm -rf ${CONF}/pkg-list-dir || { echo "${ERROR}: rm: failed to execute rm -rf liblistdir" ; endscript; }

mkdir -p ${CONF}/pkg-list-dir || { echo "${ERROR}: could not make directory liblistdir" ; endscript; }

mkdir -p ${TEMP}/temp-pkg-list-dir || { echo "${ERROR}: could not make directory temp/liblist-tempdir" ; endscript; }

git clone "$LIST_REMOTE" ${TEMP}/temp-pkg-list-dir || { echo "${ERROR}: git: could not clone LIBREPO" ; endscript; }

echo "formatting the list..."

PKGLISTTEMP=${TEMP}/temp-pkg-list-dir/${LIST_FILENAME}
mv "$PKGLISTTEMP" "$PKGLIST"
format-file-in-place "$PKGLIST"
remove-first-n-lines-from-file 3 "$PKGLIST"

echo "done"

endscript

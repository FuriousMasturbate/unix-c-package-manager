#!/bin/bash

RED='\033[0;31m'
NC='\033[0m' # no color

ERROR=${RED}error${NC}
DONE=${RED}done${NC}

USER_HOME=$(eval echo ~${SUDO_USER})
USER_NAME=$(eval echo ${SUDO_USER})

#eval sudo -u $USER

rm -f /usr/local/bin/cpkg || { echo -e "${ERROR}: rm: failed to execute 'rm -f /usr/local/bin/ridi'" ; exit 1; }
rm -rf /usr/local/bin/cpkg-sh || { echo -e "${ERROR}: rm: failed to execute 'rm -rf /usr/local/bin/sh'" ; exit 1; }
sudo -u "$USER_NAME" rm -rf "${USER_HOME}/.config/cpkg" || { echo -e "${ERROR}: problem" ; exit 1; }

cp unix-cpkg/cpkg.sh /usr/local/bin/cpkg || { echo -e "${ERROR}: rm: failed to execute 'cp ridi.sh /usr/local/bin/ridi'" ; exit 1; }
cp -r unix-cpkg/sh /usr/local/bin/cpkg-sh || { echo -e "${ERROR}: rm: failed to execute 'cp -r sh /usr/local/bin/sh'" ; exit 1; }
sudo -u "$USER_NAME" mkdir -p "${USER_HOME}/.config/cpkg" || { echo -e "${ERROR}: problem" ; exit 1; }
sudo -u "$USER_NAME" cp unix-cpkg/lib-update.conf "${USER_HOME}/.config/cpkg" || { echo -e "${ERROR}: problem" ; exit 1; }

echo -e "$DONE"
exit 0
